#!/bin/sh
# autopkgtest check: Split and check a Kubernetes yaml manifest file against
# kubernetes-split-yaml, to verify that the file was splitted correctly.
# Author: Arthur Diniz <arthurbdiniz@gmail.com>

set -e

TEST_TMP="${AUTOPKGTEST_TMP:-/tmp}/kubernetes-split-yaml"

mkdir -p "${TEST_TMP}"
cp -r debian/tests/namespaced "${TEST_TMP}"
cd "${TEST_TMP}"

kubernetes-split-yaml --template_sel tpl_ns namespaced/namespaced-k8s-file.yaml

if [ ! -f generated/my-nginx-1/my-nginx-1.Deployment.yaml ]; then
  exit 1
fi

diff generated/my-nginx-1/my-nginx-1.Deployment.yaml namespaced/my-nginx-1-deployment.yaml
echo "check-ns-my-nginx-1-deployment: OK"

if [ ! -f generated/my-nginx-1/my-nginx-1.Service.yaml ]; then
  exit 1
fi

diff generated/my-nginx-1/my-nginx-1.Service.yaml namespaced/my-nginx-1-service.yaml
echo "check-ns-my-nginx-1-service: OK"

if [ ! -f generated/my-nginx-2/my-nginx-2.Deployment.yaml ]; then
  exit 1
fi

diff generated/my-nginx-2/my-nginx-2.Deployment.yaml namespaced/my-nginx-2-deployment.yaml
echo "check-ns-my-nginx-2-deployment: OK"

if [ ! -f generated/my-nginx-2/my-nginx-2.Service.yaml ]; then
  exit 1
fi

diff generated/my-nginx-2/my-nginx-2.Service.yaml namespaced/my-nginx-2-service.yaml
echo "check-ns-my-nginx-2-service: OK"

if [ ! -f generated/_no_ns_/my-nginx-3.Deployment.yaml ]; then
  exit 1
fi

diff generated/_no_ns_/my-nginx-3.Deployment.yaml namespaced/my-nginx-3-deployment.yaml
echo "check-no-ns-my-nginx-3-deployment: OK"

if [ ! -f generated/_no_ns_/my-nginx-3.Service.yaml ]; then
  exit 1
fi

diff generated/_no_ns_/my-nginx-3.Service.yaml namespaced/my-nginx-3-service.yaml
echo "check-no-ns-my-nginx-3-service: OK"