#!/bin/bash
# autopkgtest check: Split and check a Kubernetes yaml manifest file against
# kubernetes-split-yaml, to verify that the file was splitted correctly.
# Author: Arthur Diniz <arthurbdiniz@gmail.com>

set -e

TEST_TMP="${AUTOPKGTEST_TMP:-/tmp}/kubernetes-split-yaml"

mkdir -p "${TEST_TMP}"
cp -r debian/tests/objects-file "${TEST_TMP}"
cd "${TEST_TMP}"

kubernetes-split-yaml --name_re ^my-nginx-1 --template_sel tpl_ns objects-file/objects-k8s-file.yaml
kubernetes-split-yaml --name_re ^my-nginx-2 --template_sel tpl_ns objects-file/objects-k8s-file.yaml

if [ ! -f generated/my-nginx-1/my-nginx-1.Deployment.yaml ]; then
  exit 1
fi

diff generated/my-nginx-1/my-nginx-1.Deployment.yaml objects-file/my-nginx-1-deployment.yaml
echo "check-my-nginx-1-deployment: OK"

if [ ! -f generated/my-nginx-1/my-nginx-1.Service.yaml ]; then
  exit 1
fi

diff generated/my-nginx-1/my-nginx-1.Service.yaml objects-file/my-nginx-1-service.yaml
echo "check-my-nginx-1-service: OK"

if [ ! -f generated/my-nginx-2/my-nginx-2.Deployment.yaml ]; then
  exit 1
fi

diff generated/my-nginx-2/my-nginx-2.Deployment.yaml objects-file/my-nginx-2-deployment.yaml
echo "check-my-nginx-2-deployment: OK"

if [ ! -f generated/my-nginx-2/my-nginx-2.Service.yaml ]; then
  exit 1
fi

diff generated/my-nginx-2/my-nginx-2.Service.yaml objects-file/my-nginx-2-service.yaml
echo "check-my-nginx-2-service: OK"